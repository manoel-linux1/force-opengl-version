# FORCE-OPENGL-VERSION is no longer being maintained

####################################

- force-opengl-version: sep 2023

- build-latest: 0.0.1

# For FreeBSD

- On FreeBSD, install bash to have no issues.

- To resolve the issue of the FORCE-OPENGL-VERSION error, even with bash, use the following command: `sudo ln -s /usr/local/bin/bash /bin/

####################################

- Support for the distro: Void-Linux/Ubuntu/Debian/Arch/Artix/Manjaro/FreeBSD

- If you are using a distribution based on Ubuntu, Debian, or Arch, the script will work without any issues.

- FORCE-OPENGL-VERSION is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with FORCE-OPENGL-VERSION, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of FORCE-OPENGL-VERSION to add additional features.

## Installation

- To install FORCE-OPENGL-VERSION, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/force-opengl-version.git

# 2. To install the FORCE-OPENGL-VERSION script, follow these steps

- chmod a+x `installupdate.sh`

- sudo `./installupdate.sh`

## Without Compatibility Mode

# Version not intended for OpenJDK, designed to run a program in OpenGL mode without ES

`force-opengl-version`

# Version not intended for OpenJDK, designed to run a program in OpenGL mode with ES

`force-opengl-version-with-es`

# Version intended for OpenJDK, designed to run a .jar in OpenGL mode without ES.

`force-opengl-version-for-openjdk`

# Version intended for OpenJDK, designed to run a .jar in OpenGL mode with ES.

`force-opengl-version-with-es-for-openjdk`

## With Compatibility Profile mode

# Version not intended for OpenJDK, designed to run a program in OpenGL mode without ES

`force-opengl-version-with-compat-profile`

# Version not intended for OpenJDK, designed to run a program in OpenGL mode with ES

`force-opengl-version-with-es-compat-profile`

# Version intended for OpenJDK, designed to run a .jar in OpenGL mode without ES.

`force-opengl-version-with-compat-profile-for-openjdk`

# Version intended for OpenJDK, designed to run a .jar in OpenGL mode with ES.

`force-opengl-version-with-es-for-compat-profile-openjdk`

# For uninstall

- chmod a+x `uninstall.sh`

- sudo `./uninstall.sh`

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux/BSD experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The FORCE-OPENGL-VERSION project is currently in development. The latest stable version is 0.0.1. We aim to provide regular updates and add more features in the future.

# License

- FORCE-OPENGL-VERSION is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of FORCE-OPENGL-VERSION.